-- Ejercicio 2

entity Circuito2 is
port (A0, A1, A2, A3, B0, B1, B2, B3: in bit;
	S0, S1, S2, S3: out bit;
	C0, C1, C2, C3: inout bit);
end Circuito2;