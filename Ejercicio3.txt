-- Ejercicio 3

library IEEE;
use IEEE.std_logic_1164.all;

entity Circuito3 is
port (Q : in bit_vector (3 downto 0); F: out bit_vector (0 to 7));
end Circuito3;